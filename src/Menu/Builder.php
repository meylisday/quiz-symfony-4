<?php
/**
 * Created by PhpStorm.
 * User: meylis
 * Date: 10.08.18
 * Time: 12:31
 */

namespace App\Menu;

use Knp\Menu\FactoryInterface;

class Builder
{
    private $factory;


    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;

    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('Menu');

        $menu->addChild('Главная', ['route' => 'main']);
        $menu->addChild('Рейтинг', ['route' => 'rating']);
        $menu->setChildrenAttribute('class', 'navbar-nav mr-auto');
        foreach ($menu as $child) {
            $child->setLinkAttribute('class', 'nav-link link__color')
                ->setAttribute('class', 'nav-item');
        }

        return $menu;


    }

}