<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\GeneralInfoType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

class PersonalProfileController extends AbstractController
{
    /**
     * @Route("/user/profile/{slug}", name="profile")
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EntityManagerInterface $em, User $user)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $id = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($id);


        return $this->render('personal_profile/profile.html.twig',[
            'users' => $user,
        ]);
    }

    /**
     * @Route("user/profile/{id}/edit", name="updateInfo", requirements={"id"="\d+"})
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updatePersonalInfoAction($id, Request $request) : Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($id);
        $username = $request->query->get('username');
        $email = $request->query->get('email');
        $user->setUsername($username);
        $user->setEmail($email);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('profile');

    }

}
