<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ForgetController extends AbstractController
{
    /**
     * @Route("/forget", name="forget", host="quiz.test")
     */
    public function index()
    {
        return $this->render('forget/forget.html.twig');
    }
}
