<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Quiz;
use App\Entity\User;
use App\Form\QuestionAnswerType;
use App\Form\QuestionType;
use App\Form\QuizType;
use App\Service\Deleter\QuizDeleter;
use App\Service\FormHandler\QuizCreateFormHandler;
use App\Service\FormHandler\QuizUpdateFormHandler;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function showAllUsers(EntityManagerInterface $em, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');
        $repository = $em->getRepository(User::class);
        $users = $repository->findAll();
        if (!$users){
            throw $this->createNotFoundException("Пользователей не найдено!");

        }
        return $this->render('admin/admin.html.twig',[
            'users' => $users
        ]);
    }

    /**
     * @Route("/admin/delete/{id}", name="delete")
     */

    public function userRemoveAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($id);
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin');

    }
    /**
     * @Route("/admin/update/{id}", name="update")
     */

    public function userUpdateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->find($id);
        $roles = $user->getStatus();
        if ($roles == false){
            $user->setStatus('1');
        }else{
            $user->setStatus('0');
        }
        $em->flush();

        return $this->redirectToRoute('admin');

    }
    /**
     * @Route("/admin/quiz/create", name="quiz.create")
     *
     * @param Request $request
     * @param QuizCreateFormHandler $quizCreateFormHandler
     * @return Response
     */
    public function createQuiz(Request $request, QuizCreateFormHandler $quizCreateFormHandler): Response
    {
        $quiz = new Quiz();

        $form = $this->createForm(QuizType::class, $quiz);
        if ($quizCreateFormHandler->handle($form, $request)) {
            return $this->redirectToRoute('quiz.show');
        }

        return $this->render('quizzes/quiz_create.html.twig', [
            'form' => $form->createView(),
            'errors' => $quizCreateFormHandler->getFormErrorMessages()
        ]);
    }
    /**
     * @Route(
     *     "/admin/quiz/update/{id}",
     *     name="quiz.update",
     *     requirements={"id"="\d+"}
     * )
     *
     * @param int $id
     * @param Request $request
     * @param QuizUpdateFormHandler $quizUpdateFormHandler
     * @return Response
     */
    public function updateQuiz(int $id, Request $request, QuizUpdateFormHandler $quizUpdateFormHandler): Response
    {
        $quiz = $this
            ->getDoctrine()
            ->getRepository(Quiz::class)
            ->find($id);

        if (!$quiz) {
            throw $this->createNotFoundException('Викторины с индексом ' . $id . ' не существует');
        }

        $form = $this->createForm(QuizType::class, $quiz);
        if ($quizUpdateFormHandler->handle($form, $request, ['entity' => $quiz])) {
            return $this->redirectToRoute('quiz.show');
        }

        return $this->render('quizzes/quiz_update.html.twig', [
            'form' => $form->createView(),
            'errors' => $quizUpdateFormHandler->getFormErrorMessages()
        ]);
    }

    /**
     * @Route("/admin/quiz/delete/{id}", name="quiz.delete")
     *
     * @param int $id
     * @param QuizDeleter $quizDeleter
     * @return Response
     */
    public function deleteQuiz(int $id, QuizDeleter $quizDeleter): Response
    {
        if (!$quizDeleter->delete($id)) {
            throw $this->createNotFoundException('Викторины с индексом ' . $id . ' не существует ');
        }

        return $this->redirectToRoute('quiz.show');
    }

    /**
     * @Route("/quiz/own",name="quiz.own")
     *
     * @param Request $request =
     * @return Response
     */
    public function showOwnQuizzes(Request $request)
    {
        return new Response();
    }

    /**
     * @Route("/",name="quiz.show")
     *
     * @return Response
     */
    public function showAllQuizzes(): Response
    {
        $quizzes = $this
            ->getDoctrine()
            ->getRepository(Quiz::class)
            ->findAll();

        return $this->render('quizzes/quizzes.html.twig', [
            'quizzes' => $quizzes
        ]);
    }
}
